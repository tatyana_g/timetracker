const express        = require('express');
const bodyParser     = require('body-parser');
const app            = express();
var cors             = require('cors');
const config         = require ('./config/db.js');

app.use(cors());

const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));

var MongoClient = require('mongodb').MongoClient;

var options = {
    server: {
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 500
    }
};

var uri = config.url;


MongoClient.connect(uri, options, function(err, client) {

    const myDb = client.db("Nodeapp");

    if(err)
        console.log("Error connecting to the database", err);

    require('./app/routes')(app, myDb);

    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
});
