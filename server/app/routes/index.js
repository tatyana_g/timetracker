const create_new_entry = require('./history/create_new_entry'),
    dashboard = require('./dashboard'),
    get_history = require('./history/get_history'),
    delete_project_entry = require('./history/delete_project_entry'),
    update_project_entry = require('./history/update_project_entry'),
    update_activity_entry = require('./history/update_activity_entry'),
    delete_activity_entry = require('./history/delete_activity_entry'),
    projects = require('./projects');


module.exports = function(app, db, client) {
    create_new_entry(app, db, client);
    get_history(app, db);
    delete_project_entry(app, db);
    update_project_entry(app, db);
    update_activity_entry(app, db);
    delete_activity_entry(app, db);
    dashboard(app, db);
    projects(app, db);
}

