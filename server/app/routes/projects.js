var ObjectId = require('mongodb').ObjectId;


/*
 * All actions with projects: create, read, delete, update.
 */

module.exports = function(app, db) {

    /* Create new project */
    app.post('/api/projects', (req, res) => {

        console.log("Req.body.project: " + req.body.project);

        const newProject = {
            name: req.body.project,
            sumOfTime: Number(0)
        }

        db.collection('projects').insert(newProject, (err, result) => {
            if (err) {
                res.send('Error: an error occured during creating a new project');
                console.log(err);
            } else {
                res.send(result.ops[0]);
            }
        })
    });

    /* Get all projects */
    app.get('/api/projects', (req, res) => {

        db.collection('projects').find({}).toArray((err, results) => {
            if (err) {
                res.send("Error finding all projects");
                console.log(err);
            } else {
                res.send(results);
            }
            }
        )
    })


    /* Update certain project */
    app.put('/api/projects', (req, res) => {
        const oldProject = req.body.oldProject;
        const newProject = req.body.newProject;

        console.log("Old project: " + oldProject);
        console.log("New project: " + newProject);

        db.collection('projects').findOne({
            name: oldProject
        }, (err, doc) => {

            console.log("Found: " + doc.name);
            if (err) {
               console.log(err);
            }

            doc.name = newProject;
            console.log("Updated to: " + doc.name);

            const details = {'_id': new ObjectId(doc._id)};

            db.collection('projects').update(details, doc, (err, result) => {
                if (err) {
                    console.log(err);

                } else {

                    // update all entries in History with a new project name
                    db.collection('history').find({ "projects.project": oldProject }).toArray((err, docs) => {
                        if (err) {
                            console.log(err);
                        } else {

                            for ( let i = 0; i< docs.length; i++) {

                                const details = {'_id': new ObjectId(docs[i]._id)};


                                for (let j = 0; j < docs[i].projects.length; j++) {
                                    if (docs[i].projects[j].project == oldProject) {
                                        docs[i].projects[j].project = newProject;
                                    }
                                }

                                db.collection('history').update(details, docs[i], (err, result) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("Updated entry with id: " + docs[i]._id);
                                    }
                                })
                            }
                        }
                    })

                    console.log("Updated project " + doc.name);
                    res.send(doc);
                }
            })
        })
    })

    /* Delete certain project */
    app.delete('/api/projects', (req, res) => {

        const project = req.body.project;

        db.collection('projects').findOne({"name": project}, (err, doc) => {
            if (err)
                console.log(err);

            const details = {'_id': new ObjectId(doc._id)};

            db.collection('projects').remove(details, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Deleting project " + project)
                    res.send({"Deleted": "ok"});
                }
            })
        })
    })
}