var ObjectId = require('mongodb').ObjectId;

/*
    Update activity entry by it's id
*/

module.exports = function(app,db) {
    app.put('/api/history/activities', (req, res) => {

        const id = req.body.activityId;
        const project = req.body.project;
        const newActivityName = req.body.updatedActivity;

        db.collection('history').findOne({
            /* Find a document with received project and activity id */
            "projects": { $elemMatch : { project: project, "activities.id": id }}
        }, (err, doc) => {

            if (err) {
                console.log('An error occured during updating activity entry');
            };

            console.log(doc);

            /* Find project and activity and update activity's name */
            for (let i = 0; i < doc.projects.length; i++) {
                if (doc.projects[i].project == project) {
                    console.log(doc.projects[i].project);
                    for (let j = 0; j < doc.projects[i].activities.length; j++) {
                        if (doc.projects[i].activities[j].id === id)
                            doc.projects[i].activities[j].activity = newActivityName;
                    }
                }
            }

            const details = {'_id': new ObjectId(doc._id)};

            // Update document with a new activity
            db.collection('history').update(details, doc, (err, result) => {
                if (err) {
                    res.send('An error occured during updating activity entry');
                    console.log(err);
                }
                else {
                    res.send(doc);
                }
            } )
        })
    })
}