var ObjectId = require('mongodb').ObjectId;

module.exports = function(app, db) {
    /*
        Delete project entry by it's id
    */
    app.delete('/api/history/projects', (req, res) => {

        const id = req.body.projectId;

        // find document where projectId = id
        db.collection('history').findOne({
            "projects.id": id
        }, (err, doc) => {

            if (err) {
                console.log('An error occured during deleting project entry');
            };

            console.log(doc);
            let projectTime = 0;
            let projectName = "";

            // Subtract project's duration from day's duration
            for (let i = 0; i < doc.projects.length; i++) {
                if (doc.projects[i].id == id) {
                    // Save projectTime and projectName
                    projectTime = doc.projects[i].duration;
                    projectName = doc.projects[i].project;
                    // subtract project's duration from day's duration
                    doc.duration = doc.duration - projectTime;
                    // delete project entry
                    doc.projects.splice(i, 1);
                }
            }

            /* Subtract this project entry's time from sum of all time for this project which is stored in 'projects' collection */
            db.collection('projects').findOne({name: projectName}, (err, doc) => {

                if (err) {
                    console.log(err);
                }

                if (doc != null) {
                    doc.sumOfTime -= projectTime;
                    console.log("Removed interval: " + projectTime);
                    console.log("Doc.sumOfTime: " + doc.sumOfTime);

                    const details = {'_id': new ObjectId(doc._id)};

                    db.collection('projects').update(details, doc, (err, result) => {
                        if (err)
                            console.log(err);
                    })
                }
            });


            const details = {'_id': new ObjectId(doc._id)};

            /* Update document with changes */
            if (doc.projects.length < 1) {

                console.log(doc);

                console.log("Doc.projects.length < 1");

                // if there are no more project entries for this day, delete this document
                db.collection('history').remove(details, (err) => {
                    if (err) {
                        res.send('Error: an error occured during deleting document');
                    } else {
                        res.send({"Document deleted": "true"});
                    }
                })

            } else {
                console.log(doc);

                console.log("Doc.projects.length > 1");
                // if there are other project entries, update document
                db.collection('history').update(details, doc, (err, result) => {
                    if (err) {
                        res.send('An error occured during deleting project entry');
                        console.log(err);
                    }
                    else {
                        res.send(doc);
                    }
                })
            }
        })
    })
}
