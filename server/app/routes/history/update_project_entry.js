var ObjectId = require('mongodb').ObjectId;

/*
    Update project entry by it's id
*/

module.exports = function(app, db) {
    app.put('/api/history/projects', (req, res) => {

        const id = req.body.projectId;
        const newProjectName = req.body.project;

        console.log(newProjectName);
        console.log(id);

        // find document where projectId = id
        db.collection('history').findOne({
            "projects.id": id
        }, (err, doc) => {

            if (err) {
                console.log('An error occured during updating project');
            };

            // data for changing amount of time for old project and new project
            let oldProjectName = "";
            let duration = 0;

            // update project's name
            for (let i = 0; i < doc.projects.length; i++) {
                if (doc.projects[i].id == id) {
                    oldProjectName = doc.projects[i].project;
                    duration = doc.projects[i].duration;

                    doc.projects[i].project = newProjectName;
                }
            }

            // subtract amount of time spend for this entry from old project's time and add to a new project's time
            db.collection('projects').findOne({name: oldProjectName}, (err, doc) => {

                if (err) { console.log(err); }

                try {
                    doc.sumOfTime -= duration;
                    const details = { '_id': new ObjectId(doc._id)};

                    db.collection('projects').update(details, doc, (err, result) => {
                        if (err)
                            console.log(err);
                    })
                } catch (err) {
                    console.log(err);
                }

            });

            db.collection('projects').findOne({ name: newProjectName}, (err, doc) => {

                if (err) { console.log(err); }

                doc.sumOfTime += duration;

                const details = { '_id': new ObjectId(doc._id)};

                db.collection('projects').update(details, doc, (err, result) => {
                    if (err)
                        console.log(err);
                })
            })

            const details = {'_id': new ObjectId(doc._id)};

            // update document with a new projectname
            db.collection('history').update(details, doc, (err, result) => {
                if (err) {
                    res.send('An error occured during updating document with a new projectname');
                    console.log(err);
                }
                else {
                    res.send(doc);
                }
            } )
        })
    })
}

