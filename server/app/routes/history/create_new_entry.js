var util = require('./utility');

module.exports = function (app, db) {

    /*
        Create new entry: what project and what activity has tracked the user and how long did it take.
     */
    app.post('/api/history', (req, res) => {

        const startOfTheDay = new Date();
        startOfTheDay.setHours(0);
        startOfTheDay.setMinutes(0);
        startOfTheDay.setSeconds(0);
        startOfTheDay.setMilliseconds(0);

        // Find an entry for this day if something was already tracked
        db.collection('history').findOne(
            {
                "day": { $lt: new Date()},
                "day": { $gte: startOfTheDay }

            }, (err, item) => {
            if (err) {
                res.send('Error: an error occured during finding a document for a current day');
                console.log(err);
            } else {

                // If there are no entries for today then create new one with current project and current activity
                if (item === null) {
                    util.createNewDocument(db, req, res);
                } else {
                    const itemId = item._id;

                    // If there is an entry for today, check if the selected project was tracked
                    util.projectWasTracked(db, req, itemId).then(
                        () => {
                            console.log("Project was tracked");

                            // If the project was tracked, check if the selected activity was tracked
                            util.activityWasTracked(db, req, itemId).then(
                                () => {
                                    // If it was tracked, add a new interval for it
                                    console.log("Activity was tracked");

                                    util.addNewInterval(db, req, res, itemId);
                                    }
                            ).catch(
                                () => {

                                    // If an activity wasn't tracked, add a new current activity and an interval for it
                                    console.log("Activity wasn't tracked");
                                    util.createNewActivity(db, req, res, itemId);
                                }
                            )
                        }
                    ).catch(() => {

                            // The project didn't tracked, so add it and add current activity and add new interval
                            console.log("Project didn't tracked")

                            util.createNewProject(db, req, res, itemId);
                        }
                    )
                }
            }
        });
    })
}

