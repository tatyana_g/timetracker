var moment = require('moment');
var ObjectId = require('mongodb').ObjectId;
var uniqueId = require('../../util/generateIds');

/*
    Utility functions used for creating a new entry in history.
*/

function createNewDocument(db, req, res) {

    const currentProject = req.body.project;
    const currentActivity = req.body.activity;
    const start = req.body.start;
    const stop = req.body.stop;
    const currentDay = new Date();

    const newDocument = {
        "day": currentDay,
        "projects": [
            {
                "project": currentProject,
                "activities": [
                    {
                        "activity": currentActivity,
                        "duration": 0,
                        "intervals": [
                            {
                                start: start,
                                stop: stop
                            }
                        ],
                        "id": uniqueId()
                    }
                ],
                "duration": 0,
                "id": uniqueId()
            }
        ],
        "duration": 0
    }

    console.log("Document for current date is null, create new one.");

    db.collection('history').insert(newDocument, (err, result) => {

        if (err) {
            res.send('Error: an error occured during creating new document for a day.');
            console.log(err);
        }
        else {
            updateDurations(db, req, res, newDocument);
        }
    })
}


/*
    Check if the project was already used this day.
 */

function projectWasTracked(db, req, itemId) {

    const currentProject = req.body.project;

    console.log("Checking if the current project was already tracked today");

    var isTracked = false;

    function changeIsTracked() {
        isTracked = !isTracked;
    }

    return new Promise((resolve, reject) => {
        findCurrentProject(itemId, currentProject, db)
            .then(() => {
                changeIsTracked()
                if (isTracked) {
                    resolve();
                }
            })
            .catch(() => {
                reject();
            })
    });
}

/*
    Perform a database query to check if a selected project was tracked today.
 */

function findCurrentProject(itemId, currentProject, db) {

    return new Promise((resolve, reject) => {

        db.collection('history').find({
                "_id": itemId,
                "projects.project": currentProject
            }
        ).toArray((err, doc) => {
            if (err) {
                reject("Error occured while finding current project");
            }
            if (doc.length > 0) {
                resolve();
            } else {
                reject("Project for this day doesn't exist");
            }
        });
    })
}

/*
    Check if this activity was tracked this day.
 */

function activityWasTracked(db, req, itemId) {

    const currentActivity = req.body.activity;
    const currentProject = req.body.project;

    console.log("Checking if the current activity was tracked today");

    var isTracked = false;

    module.exports = function changeIsTracked() {
        isTracked = !isTracked;
        // console.log("Went to then");
        console.log("changeIsTracked module.exports = function: " + isTracked);
    }

    return new Promise((resolve, reject) => {
        findCurrentActivity(itemId, currentProject, currentActivity, db)
            .then(() => {
                changeIsTracked()
                if (isTracked) {
                    resolve();
                }
            })
            .catch(() => {
                reject();
            })
    });
}

/*
    Perform a database query to check if selected activity was tracked today.
 */

function findCurrentActivity(itemId, currentProject, currentActivity, db) {

    return new Promise((resolve, reject) => {

        // find if the selected activity was tracked today
        db.collection('history').find({
                "_id": itemId,
                // find a field 'project' = currentProject and a field 'activity' = currentActivity
                "projects": { $elemMatch : { project: currentProject, "activities.activity": currentActivity }}
            }
        ).toArray((err, doc) => {
            console.log(doc);
            if (doc.length > 0) {
                resolve();
            } else {
                reject("Activity for this day doesn't exist");
            }
        });
    })
}

/*
    Create a new project entry in history.
 */

function createNewProject(db, req, res, itemId) {

    const currentProject = req.body.project;
    const currentActivity = req.body.activity;
    const start = req.body.start;
    const stop = req.body.stop;

    db.collection('history').findOne({"_id": itemId}, (err, doc) => {

        if (err)
            console.log('Error finding document');
        else {
            // add project
            const newProject = {
                "project": currentProject,
                "activities": [
                    {
                        "activity": currentActivity,
                        "duration": 0,
                        "intervals": [
                            {
                                start: start,
                                stop: stop
                            }
                        ],
                        "id": uniqueId()
                    }
                ],
                "duration": 0,
                "id": uniqueId()
            };

            console.log(doc.projects);
            doc.projects.push(newProject);
            db.collection('history').update({"_id": itemId}, doc, (err) => {
                if (err)
                    res.send({'error': 'An error occured during creating new project'});
                else
                    updateDurations(db, req, res, doc);
            })
        }
    })
}

/*
    Create a new activity entry in history.
 */

function createNewActivity(db, req, res, itemId) {

    const currentProject = req.body.project;
    const currentActivity = req.body.activity;
    const start = req.body.start;
    const stop = req.body.stop;

    db.collection('history').findOne({"_id": itemId}, (err, doc) => {

        if (err)
            console.log('Error finding document');
        else {
            // Add activity
            const newActivity = {
                "activity": currentActivity,
                "duration": 0,
                "intervals": [
                    {
                        start: start,
                        stop: stop
                    }
                ],
                "id": uniqueId()
            };

            console.log(doc.projects);
            for (var i = 0; i < doc.projects.length; i++) {
                if (doc.projects[i].project == currentProject) {
                    console.log("Found project for adding activity: " + doc.projects[i].project);
                    doc.projects[i].activities.push(newActivity);
                }
            }


            db.collection('history').update({"_id": itemId}, doc, (err, result) => {
                if (err)
                    res.send('An error occured during creating new activity');
                else
                    updateDurations(db, req, res, doc);
            })
        }
    })
}

/*
    Add a new interval of time in case that current project and activity were tracked this day.
 */

function addNewInterval(db, req, res, itemId) {

    const currentProject = req.body.project;
    const currentActivity = req.body.activity;
    const start = req.body.start;
    const stop = req.body.stop;

    db.collection('history').findOne({"_id": itemId}, (err, doc) => {
        if (err)
            console.log('Error finding document to add new interval');
        else {

            let amountOfIntervals = 0;

            let projectIndex;
            let activityIndex;

            // find project's index and activity's index and find how many intervals are there for current project and activity
            for (var i = 0; i < doc.projects.length; i++) {
                if (doc.projects[i].project == currentProject) {
                    projectIndex = i;

                    for (var j = 0; j < doc.projects[i].activities.length; j++) {
                        if (doc.projects[i].activities[j].activity == currentActivity) {
                            activityIndex = j;

                            console.log("Found activity: " + doc.projects[i].activities[j].activity);
                            amountOfIntervals = doc.projects[i].activities[j].intervals.length;

                            console.log("Amount of intervals: " + amountOfIntervals);
                        }
                    }
                }
            }

            // Path to 'intervals' field in MongoDB
            const intervals = doc.projects[projectIndex].activities[activityIndex].intervals;

            intervals[amountOfIntervals] = {
                start: start,
                stop: stop
            }

            updateDurations(db, req, res, doc);
        }
    })
}

/*
    Update duration for a project and a day after adding a new activity entry.
 */

function updateDurations(db, req, res, doc) {

    let projectIndex = 0;
    let activityIndex = 0;
    let amountOfIntervals = 0;
    const currentProject = req.body.project;
    const currentActivity = req.body.activity;

    // Find project's index, activity's index and find how many intervals activity has
    for (var i = 0; i < doc.projects.length; i++) {
        if (doc.projects[i].project == currentProject) {
            projectIndex = i;

            for (var j = 0; j < doc.projects[i].activities.length; j++) {
                if (doc.projects[i].activities[j].activity == currentActivity) {
                    activityIndex = j;

                    console.log("Found activity: " + doc.projects[i].activities[j].activity);
                    amountOfIntervals = doc.projects[i].activities[j].intervals.length;

                    console.log("Amount of intervals: " + amountOfIntervals);
                }
            }
        }
    }

    // Path to 'intervals' field in the document
    const intervals = doc.projects[projectIndex].activities[activityIndex].intervals;

    console.log("All intervals:");
    console.log(intervals);

    // 1. Set new duration for current activity

    // time of current activity in seconds for displaying it in the History
    let activityTime = 0;

    const details = { '_id': new ObjectId(doc._id)};

    // count the sum of all intervals
    for (let i = 0; i < amountOfIntervals; i++) {
        let stop = moment(intervals[i].stop);
        let start = moment(intervals[i].start);
        // count difference in sec, then format it to number
        const intervalTime = Number(moment(stop.diff(start)));
        activityTime += Math.floor(intervalTime / 1000);
    }

    console.log("Activity duration in seconds is: " + activityTime);

    doc.projects[projectIndex].activities[activityIndex].duration = activityTime;

    // 2. Set duration for current project

    let projectTime = 0;
    const amountOfActivities = doc.projects[projectIndex].activities.length;

    for (let i = 0; i < amountOfActivities; i++) {
        projectTime += doc.projects[projectIndex].activities[i].duration;
    }

    console.log("Current project time: " + projectTime);
    doc.projects[projectIndex].duration = projectTime;


    // 3. Set duration for current day

    let dayTime = 0;
    const amountOfProjects = doc.projects.length;
    for (let i = 0; i < amountOfProjects; i++) {
        dayTime += doc.projects[i].duration;
    }

    console.log("Current day time: " + dayTime);
    doc.duration = dayTime;

    /* Add tracked time to sum of time for current project */
    db.collection('projects').findOne({name: currentProject}, (err, doc) => {

        if (err) { console.log(err); }

        try {
            doc.sumOfTime += activityTime;
            console.log("Added interval: " + activityTime);

            const details = {'_id': new ObjectId(doc._id)};

            db.collection('projects').update(details, doc, (err, result) => {
                if (err)
                    console.log(err);
            })
        } catch (err) {
            console.log(err);
        }
    });

    // Update document
    db.collection('history').update(details, doc, (err, result) => {
        if (err) {
            res.send('An error occured during updating document while counting duration');
            console.log(err);
        }
        else {
            res.send(doc);
        }
    })
}



module.exports =  {
    addNewInterval: addNewInterval,
    createNewActivity: createNewActivity,
    createNewProject: createNewProject,
    activityWasTracked: activityWasTracked,
    projectWasTracked: projectWasTracked,
    createNewDocument: createNewDocument
};
