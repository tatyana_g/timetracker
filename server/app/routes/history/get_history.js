var ObjectId = require('mongodb').ObjectId;

/*
     *  Find all history of tracked projects and activities.
     *  One document represents a day:
     *  {
     *      day: "Nov 20th 2018",
     *      projects: [
     *          {   project: "My project",
     *              activities: [
     *                  {   activity: "Creating new features",
     *                      duration: 10,
     *                      intervals: [
     *                          {   start: 'ISODate',
     *                              stop: 'ISODate'
     *                          },
     *                          {   start: 'ISODate',
     *                              stop: 'ISODate'
     *                          }
     *                      ]
     *                  }
     *              ]
     *          }
     *      ]
     *  }
     *
*/

module.exports = function(app, db) {
    app.get('/app', (req, res) => {

        db.collection('history').find().sort({ day: -1}).toArray((err, docs) => {
            if (err) {
                res.send("Error: an error occured during finding a history");
                console.log(err);
            } else {
                // res.send(docs.splice(0, 10));
                res.send(docs);
            }
        })
    })
}