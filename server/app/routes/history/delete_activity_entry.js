var ObjectId = require('mongodb').ObjectId;

/*
    Delete activity entry by it's id
*/

module.exports = function(app, db) {
    app.delete('/api/history/activities', (req, res) => {

        const id = req.body.activityId;
        // project name
        const project = req.body.project;

        db.collection('history').findOne({
            /* Find a document with received project and activity id */
            "projects": {$elemMatch: { project: project, "activities.id": id }}
        }, (err, doc) => {

            if (err) {
                console.log('An error occured during deleting activity entry');
            }

            console.log(doc);

            let projectName = "";
            let activityDuration = 0;
            let projectIndex = 0;

            /* Find a project and it's activity and then remove this activity */
            for (let i = 0; i < doc.projects.length; i++) {
                if (doc.projects[i].project == project) {
                    console.log(doc.projects[i].project);
                    for (let j = 0; j < doc.projects[i].activities.length; j++) {
                        if (doc.projects[i].activities[j].id === id) {

                            // save project's name and activity's duration
                            projectName = doc.projects[i].project;
                            activityDuration = doc.projects[i].activities[j].duration;
                            projectIndex = i;
                            // subtract activity's duration from day's duration
                            doc.duration = doc.duration - doc.projects[i].activities[j].duration;
                            // subtract activity's duration from project's duration
                            doc.projects[i].duration = doc.projects[i].duration - doc.projects[i].activities[j].duration;
                            // remove activity entry
                            doc.projects[i].activities.splice(j, 1);
                        }
                    }
                }
            }

            /* Subtract this entry's time from sum of all time for the project to which it belongs to */

            db.collection('projects').findOne({name: projectName}, (err, doc) => {

                if (err) { console.log(err); }

                doc.sumOfTime -= activityDuration;
                console.log("Removed interval: " + activityDuration);
                console.log("Doc.sumOfTime: " + doc.sumOfTime);

                const details = { '_id': new ObjectId(doc._id)};

                db.collection('projects').update(details, doc, (err, result) => {
                    if (err)
                        console.log(err);
                })
            });

            const details = {'_id': new ObjectId(doc._id)};

            /* Update document with changes */
            if (doc.projects[projectIndex].activities < 1) {

                console.log(doc);

                console.log("activities.length < 1");

                // if there are no more activitiy entries for this project, delete this project from the document
                doc.projects.splice(projectIndex, 1);

                if (doc.projects.length < 1) {
                    // if there are no more projects, delete the document too
                    db.collection('history').remove(details, (err) => {
                        if (err) {
                            res.send('Error: an error occured during deleting document');
                        } else {
                            res.send({"Document deleted": "true"});
                        }
                    })
                } else {
                    // if there are other projects in thic document, just update it
                    db.collection('history').update(details, doc, (err) => {
                        if (err) {
                            res.send('Error: an error occured during deleting document');
                        } else {
                            res.send(doc);
                        }
                    })
                }

            } else {

                console.log("activities.length > 1");
                // if there are other activity entries in project, update project
                db.collection('history').update(details, doc, (err, result) => {
                    if (err) {
                        res.send('An error occured during deleting project entry');
                        console.log(err);
                    }
                    else {
                        res.send(doc);
                    }
                })
            }
        })
    })
}