var moment = require('moment');

/*
 * Provides analytics for the dashboard.
 */

module.exports = function(app, db) {

    /*
     * Responds with data for a chart in the following form:
     * {
     *      projects: [ 'project 1', ... ],
     *      data:
     *          [ { "day" : date,
     *              "project 1" : duration,
     *              "project 2" : duration,
     *              ...
     *              },
     *          ...]
     * }
     */
    app.get('/api/dashboard', (req, res) => {

        // Data is queried from a certain period of time for one or all projects
        const startDate = req.query.startDate;
        const endDate = req.query.endDate;
        const project = req.query.project;

        console.log("start date: " + startDate);
        console.log("end date: " + endDate);

        // Query all entries from the start date to the end date
        db.collection('history').find({ $and: [ {"day": { $gte: new Date(startDate)} }, { "day": { $lt: new Date(endDate)} } ]
        }).toArray((err, docs) => {
            if (err) {
                console.log(err);
            } else {
                var responseData = {
                    projects: [],
                    data: []
                };

                // If a certain project was selected then create response with only this project
                if (project != "all") {
                    console.log("Selected project = " + project);

                    console.log("Amount of documents found: " + docs.length);

                    for (var i = 0; i < docs.length; i++) {

                        // find an index of this project in doc.projects array
                        var projectIndex;
                        for (var j = 0; j < docs[i].projects.length; j++) {
                            if (docs[i].projects[j].project == project) {
                                projectIndex = j;
                            }
                        }

                        if (projectIndex !== undefined) {
                            var dayEntry = {
                                day: moment(docs[i].day).format("MMM Do")
                            }
                            dayEntry[project] = docs[i].projects[projectIndex].duration;
                            responseData.data.push(dayEntry);
                        }
                        projectIndex = undefined;

                    }
                    responseData.projects.push(project);
                    console.log(responseData);

                    // If all projects were selected create response with all projects and their durations
                } else {
                        console.log("Amount of documents found: " + docs.length);
                        for (var k = 0; k < docs.length; k++) {

                            // will be pushed to responseData
                            var dayEntry = {
                                day: moment(docs[k].day).format("MMM Do")
                            }

                            // add all project names and their durations from the document to the dayEntry
                            for (var m = 0; m < docs[k].projects.length; m++) {
                                dayEntry[docs[k].projects[m].project] = docs[k].projects[m].duration;
                                if (!responseData.projects.includes(docs[k].projects[m].project))
                                    responseData.projects.push(docs[k].projects[m].project);
                            }
                            responseData.data.push(dayEntry);
                        }
                }
                console.log(responseData);

                res.send(responseData);
            }
        })
    })
}