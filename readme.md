
## Time tracker

Managing your time solution. [Demo](http://35.178.244.83/).

### Includes 

* React v.16
* Redux v.3
* React-router 
* Redux-thunk
* Jest
* Enzyme
* Lodash
* NodeJS
* Express v. 4
* MongoDB v. 4.2


Deployed using MongoDB Atlas (cloud database) and Amazon LightSail.  


### Features 
* Track time by projects and subjects 
* Show history of tracking
* Update/delete history entries
* Manage projects
* Show statistics on each project in a chart for a selected period of time 


### How to run 
* ```cd timetracker```
* ``` yarn run install-all ```
* ``` yarn run dev ```
* Go to [localhost:3000/](http://localhost:3000/)  
 
 






