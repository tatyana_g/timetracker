import {combineReducers} from 'redux';
import projects from './Projects';
import tracking from './Tracking';
import toggle from './Toggle';
import history from './History';
import dashboard from './Dashboard';
import error from './ErrorBoundary';

export default combineReducers({
    projects,
    tracking,
    toggle,
    history,
    dashboard,
    error
})