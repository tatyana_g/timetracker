import * as actionTypes from '../../constants/actionTypes';

const initialState = {
    selectProjectIsShown: {},
    activitiesAreShown: {},
    editMenuIsShown: {},
    shouldShowSelectProject: false,
    shouldShowCalendar: false
}

export default function(state = initialState, action) {
    switch (action.type) {

        case actionTypes.SHOW_SELECT_PROJECT_MENU:
            return toggleSelectProject(state, action.id);

        case actionTypes.HIDE_SELECT_PROJECT_MENU:
            return toggleSelectProject(state, action.id);

        case actionTypes.TOGGLE_ACTIVITY_BLOCK:
            return toggleActivityBlock(state, action.activitiesId)

        case actionTypes.SHOW_EDIT_MENU:
            return showEditMenu(state, action.blockId)

        case actionTypes.HIDE_EDIT_MENU:
            return hideEditMenu(state, action.blockId)

        case actionTypes.SHOW_EDIT_PROJECT_MENU:
            return showEditProjectMenu(state)

        case actionTypes.HIDE_EDIT_PROJECT_MENU:
            return closeEditProjectMenu(state)

        case actionTypes.SHOW_CALENDAR:
            return showCalendar(state)

        case actionTypes.HIDE_CALENDAR:
            return hideCalendar(state)

        default:
            return state;
    }
}

/*  Opens or closes select project menu for specific object: main-menu or project entry in history,
    which is identified by it's id.
*/
function toggleSelectProject(state, id) {

    // If invoked from main SelectProject menu on the header, then id will be equal to "main-menu"
    // If invoked from one of projects from history, then id will be equal to this project's id.

    let obj = {
        selectProjectIsShown: { }
    }
    Object.assign(obj.selectProjectIsShown, state.selectProjectIsShown);

    if (state.selectProjectIsShown[id] != undefined) {
        obj.selectProjectIsShown[id] = !state.selectProjectIsShown[id];
    } else {
        obj.selectProjectIsShown[id] = true;
    }

    return Object.assign({}, state, obj);
}

/* Opens or closes activities for specific project entry */
function toggleActivityBlock(state, activitiesId) {

    let obj = {
        activitiesAreShown: { }
    }

    Object.assign(obj.activitiesAreShown, state.activitiesAreShown)

    if (state.activitiesAreShown[activitiesId] != undefined ) {
        obj.activitiesAreShown[activitiesId] = !state.activitiesAreShown[activitiesId];
    }
    else {
        obj.activitiesAreShown[activitiesId] = true;
    }

    return Object.assign({}, state, obj)
}

/* Shows edit menu for specific project entry or project in the projects section */
function showEditMenu(state, blockId) {

    let obj = {
        editMenuIsShown: { }
    }

    Object.assign(obj.editMenuIsShown, state.editMenuIsShown)

    obj.editMenuIsShown[blockId] = true;

    return Object.assign({}, state, obj);
}

function hideEditMenu(state, blockId) {

    let obj = {
        editMenuIsShown: { }
    }

    Object.assign(obj.editMenuIsShown, state.editMenuIsShown)

    obj.editMenuIsShown[blockId] = false;

    return Object.assign({}, state, obj);
}

/* Allows user to edit project for chosen entry by using select project menu */
function showEditProjectMenu(state) {
    return {
        ...state,
        shouldShowSelectProject: true
    }
}

function closeEditProjectMenu(state) {
    return {
        ...state,
        shouldShowSelectProject: false
    }
}

/* Shows calendar input in dashboard section */
function showCalendar(state) {
    return {
        ...state,
        shouldShowCalendar: true
    }
}

/* Hides calendar input */
function hideCalendar(state) {
    return {
        ...state,
        shouldShowCalendar: false
    }
}