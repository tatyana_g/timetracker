import * as actionTypes from '../../constants/actionTypes';
import merge from 'lodash/merge';

const initialState = {
    allEntries: ''
};

export default function(state = initialState, action) {
    switch(action.type) {
        case actionTypes.FETCH_HISTORY:
            return fetchHistory(state, action.payload);
        default:
            return state;
    }
};

function fetchHistory(state, history) {
    const obj = {
        allEntries: history
    };

    console.log("FetchHistory: ");
    console.log(history);

    return Object.assign({}, state, obj);
};