import * as actionTypes from '../../constants/actionTypes';

const initialState = {
    projects: [],
    selectedProject: ''
}

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.FETCH_PROJECTS:
            return fetchProjects(state, action.payload);
        case actionTypes.SELECT_PROJECT:
            return selectProject(state, action.project);
        default:
            return state;
    }
}

function selectProject(state, project) {
    const obj = {
        selectedProject: project
    };
    console.log("selected project: " + project);
    return Object.assign({}, state, obj);
}

function fetchProjects(state, projects) {
    const obj = {
        projects
    };
    return Object.assign({}, state, obj);
}