import * as actionTypes from '../../constants/actionTypes';

const initialState = {
    error: null,
    errorInfo: null
}

export default function(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SET_ERROR:
            return Object.assign({}, state, {
                error: action.error,
                errorInfo: action.errorInfo
            })
        default:
            return state;
    }
}