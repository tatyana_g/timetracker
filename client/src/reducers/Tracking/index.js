import * as actionTypes from '../../constants/actionTypes';
import moment from "moment";

const initialState = {

    // shows whether timer is currently tracking or not
    isTracking: false,

    // an activity that was entered in the field "What are you going to do"
    currentActivity: '',

    // the amount of time since start
    // runs from null every time the user starts tracking activity
    timer: '00:00:00',

    start: '',

    stop: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case actionTypes.START_TRACKING:
            return startTracking(state, action.activity);
        case actionTypes.STOP_TRACKING:
            return stopTracking(state, action.stop);
        case actionTypes.TICK:
            return tick(state, action.timer);
        default:
            return state;
    }
}

function startTracking(state, activity) {

    const object = {
        isTracking: true,
        currentActivity: activity,
        start: moment().toISOString(),
        stop: ''
    };

    return Object.assign({}, state, object);
}

function stopTracking(state) {
    console.log("Action stopTracking");
    const obj = {
        isTracking: false,
        timer: '00:00:00',
        currentActivity: '',
        start: '',
        stop: moment().toISOString()
    }

    return Object.assign({}, state, obj);
}

function tick(state, timer) {
    const obj = {
        timer: timer
    }
    return Object.assign({}, state, obj);
}


