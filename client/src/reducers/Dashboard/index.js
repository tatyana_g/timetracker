import * as actionTypes from '../../constants/actionTypes';

const initialState = {
    data: [],
    projects: [],
    range: {
        from: undefined,
        to: undefined
    }
}

export default function(state = initialState, action) {
    switch(action.type) {
        case actionTypes.FETCH_ENTRIES_FOR_CHART:
            return fetchDataForChart(state, action.payload);
        case actionTypes.SET_RANGE:
            return Object.assign({}, state, {
                range: action.range
                }
            )
        default:
            return state;
    }
}

function fetchDataForChart(state, payload) {

    return Object.assign({}, state, {
        data: payload.data,
        projects: payload.projects
    })
}
