import * as url from "../../constants/urls";
import * as actionTypes from "../../constants/actionTypes";

export function fetchEntries(startDate, endDate, project) {
    return dispatch => {
        var params = {
            startDate: startDate,
            endDate: endDate,
            project: project
        };

        var query = [];
        for (var property in params) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(params[property]);
            query.push(encodedKey + "=" + encodedValue);
        }
        query = query.join("&");
        console.log('/api/dashboard?' + query);

        fetch(url.main + '/api/dashboard?' + query)
            .then((response) => {
                if (response.status != 200) {
                    console.log("Fetch entries response status is " + response.status);
                    return;
                }
                response.json().then(data => {
                    console.log(data);
                    dispatch({
                        type: actionTypes.FETCH_ENTRIES_FOR_CHART,
                        payload: data
                    })
                })
            })
            .catch((err) => {
                console.log("fetchAllEntries error " + err);
            })
    }
}

export function setRange(range) {
    return {
        type: actionTypes.SET_RANGE,
        range
    }
}