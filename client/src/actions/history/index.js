import * as url from '../../constants/urls';
import * as actionTypes from '../../constants/actionTypes';

/*
    Fetch all history of tracking entries from the database.
 */
export function fetchAllEntries() {
    return dispatch => {
        fetch(url.main + '/app')
            .then((response) => {
                if (response.status != 200) {
                    console.log("FetchAllEntries response status is " + response.status);
                    return;
                }
                response.json().then(data => {
                    console.log("Fetch entries completed");
                    dispatch({
                        type: actionTypes.FETCH_HISTORY,
                        payload: data
                    })
                })
            })
            .catch((err) => {
                console.log("fetchAllEntries error " + err);
            })
    }
}

/*
    Create new entry with timestamp for start/stop.
 */

export function addNewEntry(currentProject, currentActivity, start, stop) {

    return dispatch => {
        console.log("addNewEntry function. CurrentProject: " + currentProject);
        console.log("currentActivity: " + currentActivity);
        console.log("start: " + start);
        console.log("stop: " + stop);
        const payload = {
            project: currentProject,
            activity: currentActivity,
            start,
            stop
        };

        var formBody = [];
        for (var property in payload) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(payload[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(url.main + '/api/history', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("addNewEntry response status is " + response.status);
                return;
            }
            response.json().then(data => {
                console.log(data);
            }).then(() => {
                dispatch(fetchAllEntries());
            }).catch((err) => {
                console.log("addNewEntry error " + err);
            })
        });
    }
}

export function removeProjectEntry(projectId) {
    return dispatch => {

        var formBody = [];
        var encodedKey = encodeURIComponent("projectId");
        var encodedValue = encodeURIComponent(projectId);
        formBody.push(encodedKey + "=" + encodedValue);

        fetch(url.main + '/api/history/projects', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("DeleteProjectEntry response status is " + response.status);
                return;
            }
            response.json().then(() => {
                dispatch(fetchAllEntries());
            }).catch((err) => {
                console.log("DeleteProjectEntry error " + err);
            })
        });
    }
}

export function updateProjectEntry(projectId, project) {

    return dispatch => {

        const payload = {
            projectId,
            project
        }

        var formBody = [];
        for (var property in payload) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(payload[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(url.main + '/api/history/projects', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("EditProjectEntry response status is " + response.status);
                return;
            }
            response.json().then(() => {
                dispatch(fetchAllEntries());
            }).catch((err) => {
                console.log("EditProjectEntry error " + err);
            })
        });
    }
}

export function removeActivityEntry(activityId, project) {

    return dispatch => {

        var payload = {
            activityId,
            project
        }

        var formBody = [];
        for (var property in payload) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(payload[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(url.main + '/api/history/activities', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("DeleteActivityEntry response status is " + response.status);
                return;
            }
            response.json().then(() => {
                dispatch(fetchAllEntries());
            }).catch((err) => {
                console.log("DeleteActivityEntry error " + err);
            })
        });
    }
}

export function updateActivityEntry(activityId, project, updatedActivity) {

    return dispatch => {

        const payload = {
            activityId,
            project: project.project,
            updatedActivity
        };

        var formBody = [];
        for (var property in payload) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(payload[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(url.main + '/api/history/activities', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("EditActivityEntry response status is " + response.status);
                return;
            }
            response.json().then(() => {
                dispatch(fetchAllEntries());
            }).catch((err) => {
                console.log("EditActivityEntry error " + err);
            })
        });
    }
}

