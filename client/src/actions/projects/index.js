import * as actionTypes from '../../constants/actionTypes';
import * as url from '../../constants/urls';
import {fetchAllEntries} from "../history";

/*
* All actions with projects.
*/

export function selectProject(project) {
    return {
        type: actionTypes.SELECT_PROJECT,
        project
    }
};

export function fetchProjects() {
    return dispatch => {
        fetch(url.main + '/api/projects')
            .then((response) => {
                if (response.status != 200) {
                    console.log("FetchProjects response status is " + response.status);
                    return;
                }
                response.json().then(data => {
                    console.log("FetchProjects completed");
                    console.log(data);
                    dispatch({
                        type: actionTypes.FETCH_PROJECTS,
                        payload: data
                    })
                })
            })
            .catch((err) => {
                console.log("fetchAllEntries error " + err);
            })
    }
}

export function saveNewProject(project) {
    return dispatch => {

        var formBody = [];
        var encodedKey = encodeURIComponent("project");
        var encodedValue = encodeURIComponent(project);
        formBody.push(encodedKey + "=" + encodedValue);

        fetch(url.main + '/api/projects', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("saveNewProject response status is " + response.status);
                return;
            }
            response.json().then(data => {
                console.log(data);
            }).then(() => {
                dispatch(fetchProjects());
            }).catch((err) => {
                console.log("Add new project error: " + err);
            })
        });
    }
}

export function updateProject(oldProject, newProject) {
    return dispatch => {
        const payload = {
            oldProject,
            newProject
        };
        console.log(oldProject);
        console.log(newProject);

        var formBody = [];
        for (var property in payload) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(payload[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(url.main + '/api/projects', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("updateProject response status is " + response.status);
                return;
            }
            response.json().then(data => {
                console.log(data);
            }).then(() => {
                dispatch(fetchProjects());
            }).catch((err) => {
                console.log("Update project error: " + err);
            })
        });
    }
}

export function deleteProject (project) {
    return dispatch => {

        var formBody = [];
        const encodedKey = encodeURIComponent("project");
        const encodedValue = encodeURIComponent(project);
        formBody.push(encodedKey + "=" + encodedValue);
        formBody = formBody.join("&");

        fetch(url.main + '/api/projects', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        }).then(response => {

            if (response.status != 200) {
                console.log("EditProjectEntry response status is " + response.status);
                return;
            }
            response.json().then(() => {
                console.log("Fetching all entries after updating project entry");
                dispatch(fetchProjects());
            }).catch((err) => {
                console.log("EditProjectEntry error " + err);
            })
        });
    }
}
