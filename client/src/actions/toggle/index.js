import * as actionTypes from '../../constants/actionTypes';

/*
*  Show and hide UI elements
*/

export const showProjectSelect = (id) => {
    return {
        type: actionTypes.SHOW_SELECT_PROJECT_MENU,
        id
    }
}

export const hideProjectSelect = (id) => {
    return {
        type: actionTypes.HIDE_SELECT_PROJECT_MENU,
        id
    }
}

export const toggleActivityBlock = (activitiesId) => {
    return {
        type: actionTypes.TOGGLE_ACTIVITY_BLOCK,
        activitiesId
    }
}

export const showEditMenu = (blockId) => {
    return {
        type: actionTypes.SHOW_EDIT_MENU,
        blockId
    }
}


export const hideEditMenu = (blockId) => {
    return {
        type: actionTypes.HIDE_EDIT_MENU,
        blockId
    }
}

export const showEditProject = () => {
    return {
        type: actionTypes.SHOW_EDIT_PROJECT_MENU
    }
}

export const hideEditProject = () => {
    return {
        type: actionTypes.HIDE_EDIT_PROJECT_MENU
    }
}

export const showCalendar = () => {
    return {
        type: actionTypes.SHOW_CALENDAR
    }
}

export const hideCalendar = () => {
    return {
        type: actionTypes.HIDE_CALENDAR
    }
}






