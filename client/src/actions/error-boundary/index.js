import * as actionTypes from '../../constants/actionTypes';

export const setErrorState = (error, errorInfo) => {
    return {
        type: actionTypes.SET_RANGE,
        error,
        errorInfo
    }
}