import * as actionTypes from '../../constants/actionTypes';
import {addNewEntry} from "../history";

/*
* Actions for tracking time.
*/

var timer;

var hours = 0;
var minutes = 0;
var seconds = 0;


export function startTracking(activity) {
    return dispatch => {

        /* Start timer on the TimerPanel */
        timer = setInterval(() => {
            dispatch(tick());
        }, 1000);


        /* Save a start timestamp */
        dispatch({
            type: actionTypes.START_TRACKING,
            activity: activity
        });
    }
};

function select(state) {
    return state.tracking.stop;

}


export function stopTracking(activity, selectedProject, start) {

    return (dispatch, getState) => {

        /* Stop countdown and save a stop timestamp */
        dispatch({
            type: actionTypes.STOP_TRACKING,
        });

        const stop = select(getState());
        
        if (true) {

            /*console.log("Start timestamp: ");
            console.log(start);

            console.log("Stop timestamp: ");
            console.log(stop);*/

            /* Send data to the server */
            dispatch(addNewEntry(selectedProject, activity, start, stop));

            /* Clear timer */
            clearTimeout(timer);
            hours = minutes = seconds = 0;

        }
    }
};

/*
    Functon for timer that ticks every second
*/

function tick() {

    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }

    var time = ((hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" +
        (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds));

    return {
        type: actionTypes.TICK,
        timer: time
    }
}


