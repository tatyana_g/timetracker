import { selectProject, fetchProjects, saveNewProject, updateProject, deleteProject} from './projects';
import { startTracking, stopTracking } from './tracking';
import { showProjectSelect, hideProjectSelect, toggleActivityBlock, showEditMenu, hideEditMenu,
    showEditProject, hideEditProject, showCalendar, hideCalendar} from "./toggle/";
import { fetchAllEntries, addNewEntry, removeProjectEntry, updateProjectEntry,
    removeActivityEntry, updateActivityEntry} from "./history";
import { fetchEntries, setRange } from "./dashboard";
import { setErrorState } from "./error-boundary";

export {
    selectProject,
    fetchProjects,
    startTracking,
    stopTracking,
    showProjectSelect,
    hideProjectSelect,
    fetchAllEntries,
    addNewEntry,
    saveNewProject,
    toggleActivityBlock,
    showEditMenu,
    hideEditMenu,
    showEditProject,
    hideEditProject,
    updateProjectEntry,
    removeProjectEntry,
    updateActivityEntry,
    removeActivityEntry,
    updateProject,
    deleteProject,
    fetchEntries,
    setRange,
    showCalendar,
    hideCalendar,
    setErrorState
}