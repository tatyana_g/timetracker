import React from 'react';
import propTypes from 'prop-types';

const ProjectsTable = ({projects, editMenuIsShown, showEditMenu, updateProject, deleteProject, editProject}) => {

    ProjectsTable.propTypes = {
        editMenuIsShown: propTypes.object,
        showEditMenu: propTypes.func,
        updateProject: propTypes.func,
        deleteProject: propTypes.func,
        editProject: propTypes.func
    }

    return (
            <div className="projects-section">
                <div className="projects-section-main">
                    <div className="projects-table-header">Projects</div>
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Total time</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            projects.map(project => {
                                return (
                                    <tr key={project.name}>
                                        <td id={project.name} onKeyDown={updateProject}>{project.name}</td>
                                        <td>{new Date(project.sumOfTime * 1000).toISOString().substr(11, 8)}</td>
                                        <td name="ellipsis"><i id={project.name} className="fas fa-ellipsis-v" onClick={(e) => showEditMenu(e)}></i></td>
                                        <td className="invisible">{
                                            editMenuIsShown[project.name] ? (
                                                <div className="edit-entry-menu animated fadeIn faster">
                                                    <p className="edit-entry-menu-link" onClick={() => editProject(project.name) }>Edit</p>
                                                    <p className="edit-entry-menu-link" onClick={() => deleteProject(project.name)}>Delete</p>
                                                </div>
                                            ) : (
                                                null
                                            )
                                        }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
    )
}

export default ProjectsTable;