import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import {bindActionCreators} from "redux";
import ProjectsTable from './ProjectsTable';
import propTypes from 'prop-types';


/*
    Manages projects table at the /projects route.
 */

class ProjectsTableContainer extends React.Component {

    static propTypes = {
        projects: propTypes.arrayOf(propTypes.object),
        editMenuIsShown: propTypes.object
    }

    componentDidMount() {
        // fetch every time user opens "Projects" section because total time can be changed by this moment
        this.props.fetchProjects();
    }

    /* Show and close edit menu for projects */
    showEditMenu = (event) => {
        console.log("ShowEditMenu");
        const projectName = event.target.id;
        document.addEventListener('click', this.closeEditMenu);
        this.props.showEditMenu(projectName);
        project = projectName;
    }

    closeEditMenu = () => {
        this.props.hideEditMenu(project);
        document.removeEventListener('click', this.closeEditMenu);
    }

    /* Make the project editable */
    editProject = (projectName) => {
        const element = document.getElementById(projectName);
        element.classList.add("underline-projects");
        console.log("Underline?");
        document.addEventListener('click', () => this.hideUnderline(element));
        element.setAttribute("contentEditable", true);
    }

    hideUnderline = (element) => {
        document.removeEventListener('click', this.hideUnderline);
        element.classList.remove("underline-projects");
    }

    /* Update project */
    updateProject = (e) => {
        if (e.keyCode == 13) {
            const element = document.getElementById(project);
            console.log("Old project is " + project);
            const newProjectName = element.textContent;
            element.setAttribute("contentEditable", false);
            element.classList.remove("underline");
            this.props.updateProject(project, newProjectName);
        }
    }

    deleteProject = (project) => {
        console.log("Deleting project: " + project);
        this.props.deleteProject(project);
    }

    render() {
        return (

        <ProjectsTable
            projects={this.props.projects}
            editMenuIsShown={this.props.editMenuIsShown}
            showEditMenu={this.showEditMenu}
            updateProject={this.updateProject}
            deleteProject={this.deleteProject}
            editProject={this.editProject}
        />

        )
    }
}

// remember the project that the user picked for show/hide edit project menu
let project = "";


function mapStateToProps(state) {
    return {
        projects: state.projects.projects,
        editMenuIsShown: state.toggle.editMenuIsShown
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProjects: bindActionCreators(actions.fetchProjects, dispatch),
        showEditMenu: bindActionCreators(actions.showEditMenu, dispatch),
        hideEditMenu: bindActionCreators(actions.hideEditMenu, dispatch),
        updateProject: bindActionCreators(actions.updateProject, dispatch),
        deleteProject: bindActionCreators(actions.deleteProject, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (ProjectsTableContainer);