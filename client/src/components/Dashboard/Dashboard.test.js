import React from 'react';
import {shallow, configure} from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build/index";
import Dashboard from "./Dashboard";
import Enzyme from 'enzyme';

Enzyme.configure({ adapter: new Adapter() });

describe("Dashboard component", () => {

    const projects = [
        { _id: "1", "name": "Project 1", sumOfTime: 20}, {_id: "2", "name": "Project 2", sumOfTime: 30}
    ]

    it("should be rendered", () => {
        const wrapper = shallow(<Dashboard projects={projects} />);
        expect(wrapper.exists()).toBe(true);
    })

    it("correctly shows the selected project", () => {
        const wrapper = shallow(<Dashboard projects={projects}/>);
        wrapper.find('.select-project-for-chart').simulate('change', { target: { value: "Project 1"}});

        setTimeout(function() { // delay after change
            expect(wrapper.find('.select-project-for-chart').props().value).toEqual('Project 1');
        }, 0);
    })

    it("shows Calendar when user clicks on \"time interval\"", () => {
        const showCalendar = jest.fn();
        const wrapper = shallow(<Dashboard projects={projects} showCalendar={showCalendar} />)
        wrapper.find('.input-date-for-chart').simulate('click');
        expect(showCalendar).toHaveBeenCalledTimes(1);
    })

})