import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from "../../actions";
import { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import Dashboard from "./Dashboard";
import propTypes from 'prop-types';

class DashboardContainer extends React.Component {

    static propTypes = {
        data: propTypes.arrayOf(propTypes.object),
        projectsForChart: propTypes.arrayOf(propTypes.string),
        projects: propTypes.arrayOf(propTypes.object),
        range: propTypes.object,
        shouldShowCalendar: propTypes.bool
    }

    componentDidMount() {
        var date = new Date();

        // get data for last 2 months
        this.props.fetchEntries(new Date(date.setDate(date.getDate() - 60)), new Date(), "all");
        this.props.fetchProjects();
        document.getElementById('select-project-for-chart').value = 'all';
    }


    /* Handle select project for chart */
    handleSelect = () => {
        var project = document.getElementById("select-project-for-chart");
        const range = this.props.range;
        // if an interval was picked using calendar
        if (range.to != undefined && range.from != undefined) {
            this.props.fetchEntries(new Date(range.from), new Date(range.to), project.value);
        } else {
            // if an interval wasn't picked yet use default interval (2 months)
            const to = new Date();
            const date = new Date();
            const from = new Date(date.setDate(date.getDate() - 60));
            // set range for chart
            this.props.setRange({ from: from, to: to });
            this.props.fetchEntries(from, to, project.value);
        }
    }

    showCalendar = () => {
        this.props.showCalendar();
        document.addEventListener('click', this.hideCalendar)
    }

    hideCalendar = (event) => {
        const element = document.getElementsByClassName('calendar')[0];
            if (!element.contains(event.target)) {
                this.props.hideCalendar();
                document.removeEventListener('click', this.hideCalendar);
            }
    }

    render() {

        return (
            <Dashboard
                showCalendar={this.showCalendar}
                shouldShowCalendar={this.props.shouldShowCalendar}
                projects={this.props.projects}
                data={this.props.data}
                projectsForChart={this.props.projectsForChart}
                handleSelect={this.handleSelect}
            />
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchEntries: bindActionCreators(actions.fetchEntries, dispatch),
        fetchProjects: bindActionCreators(actions.fetchProjects, dispatch),
        showCalendar: bindActionCreators(actions.showCalendar, dispatch),
        hideCalendar: bindActionCreators(actions.hideCalendar, dispatch),
        setRange: bindActionCreators(actions.setRange, dispatch)
    }
}

function mapStateToProps(state) {
    return {
        data: state.dashboard.data,
        projectsForChart: state.dashboard.projects,
        projects: state.projects.projects,
        range: state.dashboard.range,
        shouldShowCalendar: state.toggle.shouldShowCalendar
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);