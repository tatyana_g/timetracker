import React from 'react';
import Calendar from "../Calendar";
import 'react-day-picker/lib/style.css';
import {ResponsiveBar} from "@nivo/bar/cjs/nivo-bar";
import propTypes from 'prop-types';

/*
    Manages the /dashboard page, displays a chart and and manages input controls.
 */

const Dashboard = ({showCalendar, shouldShowCalendar, projects, data, projectsForChart, handleSelect }) => {

    Dashboard.propTypes = {
        showCalendar: propTypes.func,
        handleSelect: propTypes.func
        // other props were checked in DashboardContainer
    }

    console.log("ShouldShowCalendar " + shouldShowCalendar);
    return (
        <div className="dashboard-wrap">
            {/* Input fields for interval of time and for project */}

            <div className="dashboard">
                <h1>Summary chart</h1>

                <div className="input-conditions-for-chart">


                    <div className="input-date-for-chart" onClick={() => showCalendar()}>
                        <p className="date-interval">Time interval</p>
                        <img src={require('../../styles/img/angle-arrow-down.png')} className="icon-arrow-down"/>

                        {
                            shouldShowCalendar ?
                                (
                                    <Calendar />
                                ) : null
                        }
                    </div>


                    <div className="input-project-for-chart">
                        <form>
                            <label className="input-project-for-chart-label">Select project</label>
                            <select className="select-project-for-chart" id="select-project-for-chart"
                                    name="projects" onChange={handleSelect} selected="all">
                                {projects.map(project => {
                                    return <option value={project.name} key={project._id}>{project.name}</option>
                                })}
                                <option value="all">All</option>
                            </select>
                        </form>
                    </div>
                </div>

                {/* Chart */}
                <div className="chart">
                    <ResponsiveBar
                        data={data}
                        /*keys={projectsForChart}*/
                        keys={projectsForChart}
                        indexBy="day"
                        margin={{
                            "top": 50,
                            "right": 50,
                            "bottom": 50,
                            "left": 60
                        }}
                        padding={0.3}
                        colors="yellow_green"
                        colorBy="id"
                        defs={[
                            {
                                "id": "dots",
                                "type": "patternDots",
                                "background": "inherit",
                                "color": "#38bcb2",
                                "size": 4,
                                "padding": 1,
                                "stagger": true
                            },
                            {
                                "id": "lines",
                                "type": "patternLines",
                                "background": "inherit",
                                "color": "#eed312",
                                "rotation": -45,
                                "lineWidth": 6,
                                "spacing": 10
                            }
                        ]}
                        borderColor="inherit:darker(1.6)"
                        axisBottom={{
                            "orient": "bottom",
                            "tickSize": 5,
                            "tickPadding": 5,
                            "tickRotation": 0,
                            "legend": "day",
                            "legendPosition": "center",
                            "legendOffset": 36
                        }}
                        axisLeft={{
                            "orient": "left",
                            "tickSize": 5,
                            "tickPadding": 5,
                            "tickRotation": 0,
                            "legend": "time",
                            "legendPosition": "center",
                            "legendOffset": -40
                        }}
                        labelSkipWidth={12}
                        labelSkipHeight={12}
                        labelTextColor="inherit:darker(1.6)"
                        animate={true}
                        motionStiffness={90}
                        motionDamping={15}
                        theme={{
                            "tooltip": {
                                "container": {
                                    "fontSize": "13px"
                                }
                            },
                            "labels": {
                                "textColor": "#555"
                            }
                        }}
                    />
                </div>
            </div>
        </div>
    )
}

export default Dashboard;