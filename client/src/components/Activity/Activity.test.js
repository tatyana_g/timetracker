import React from 'react';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as Enzyme from "enzyme";
import Activity from "./Activity";
import moment from "moment/moment";

Enzyme.configure({ adapter: new Adapter() });

describe("Activity component", () => {

    const activity =  {
        activity: 'Test activity',
        duration: 20,
        id: '123',
        intervals: [
            {
                start: '2018-12-23T06:03:30.737Z',
                stop: '2018-12-23T06:03:36.387Z'
            }
        ]
    }

    it("should be rendered", () => {
        const wrapper = shallow(<Activity activity={activity} editMenuIsShown={{'123': false}} />);
        expect(wrapper.exists()).toBe(true);
    })

    it("should render an interval", () => {
        const wrapper = shallow(<Activity activity={activity} editMenuIsShown={{'123': false}} />);
        const interval = moment(activity.intervals[0].start).local().format("HH:mm:ss") + " - " + moment
            (activity.intervals[0].stop).local().format("HH:mm:ss");
        expect(wrapper.find('.interval').text()).toEqual(interval);
    })

    it ("should open edit menu on click", () => {
        const showEditMenu = jest.fn();
        const wrapper = shallow(<Activity activity={activity} editMenuIsShown={{'123': false}} showEditMenu={showEditMenu} />);
        wrapper.find('.fa-ellipsis-v').simulate('click');
        setTimeout(function() { // delay after click
            expect(wrapper.find('.test').exists()).toBe(true);
        }, 0);
        expect(showEditMenu).toHaveBeenCalledTimes(1);
    })

    it ("should call editActivity on click", () => {
        const editActivity = jest.fn();
        const wrapper = shallow(<Activity activity={activity} editMenuIsShown={{'123': true}} editActivity={editActivity} />);
        wrapper.find('.edit-entry-menu-link').simulate('click');
        expect(editActivity).toHaveBeenCalledTimes(1);
    })

    it ("should call deleteActivity on click", () => {
        const deleteActivity = jest.fn();
        const wrapper = shallow(<Activity activity={activity} editMenuIsShown={{'123': true}} deleteActivity={deleteActivity} />);
        wrapper.find('.delete-entry-menu-link').simulate('click');
        expect(deleteActivity).toHaveBeenCalledTimes(1);
    })
})