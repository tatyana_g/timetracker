import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from '../../actions/index';
import Activity from './Activity';
import propTypes from 'prop-types';

/*
    This container contains logic for Activity presentational component.
 */

class ActivityContainer extends React.Component {

    static propTypes = {
        editMenuIsShown: propTypes.object,
        activity: propTypes.object,
        day: propTypes.string,
        project: propTypes.object
    }

    constructor(props) {
        super(props);
        this.editActivity = this.editActivity.bind(this);
    }

    /* Show and close edit menu for projects */
    showEditMenu = (event) => {
        // close menu after click outside
        document.addEventListener('click', this.closeEditMenu);
        this.props.showEditMenu(this.props.activity.id);
    }

    closeEditMenu = () => {
        this.props.hideEditMenu(this.props.activity.id);
        document.removeEventListener('click', this.closeEditMenu);
    }

    /* Make the selected activity editable */
    editActivity (activityId) {
        const element = document.getElementById(activityId);
        element.classList.add('underline-activities');
        document.addEventListener('click', () => this.hideUnderline(element));
        element.setAttribute("contentEditable", true);
    }

    hideUnderline = (element) => {
        document.removeEventListener('click', this.hideUnderline);
        element.classList.remove("underline-activities");
    }

    /* Update activity */
    updateActivity = (e) => {
        if (e.keyCode == 13) {
            const { activity, day, project } = this.props;
            const element = document.getElementById(activity.id);
            const newActivityName = element.textContent;
            element.setAttribute("contentEditable", false);
            this.props.updateActivityEntry(activity.id, project, newActivityName);
        }
    }

    deleteActivity = (e) => {
        const {activity, project } = this.props;
        this.props.removeActivityEntry(activity.id, project.project);
    }

    render() {
        return (
            <Activity
                editMenuIsShown={this.props.editMenuIsShown}
                updateActivity={this.updateActivity}
                showEditMenu={this.showEditMenu}
                editActivity={this.editActivity}
                deleteActivity={this.deleteActivity}
                activity={this.props.activity}
            />
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        editMenuIsShown: state.toggle.editMenuIsShown,
        activity: ownProps.activity,
        day: ownProps.day,
        project: ownProps.project
    }
}

function mapDispatchToProps(dispatch) {
    return {
        showEditMenu: bindActionCreators(actions.showEditMenu, dispatch),
        hideEditMenu: bindActionCreators(actions.hideEditMenu, dispatch),
        updateActivityEntry: bindActionCreators(actions.updateActivityEntry, dispatch),
        removeActivityEntry: bindActionCreators(actions.removeActivityEntry, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (ActivityContainer);