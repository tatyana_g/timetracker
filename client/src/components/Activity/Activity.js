import React from 'react';
import moment from "moment/moment";
import propTypes from 'prop-types';

/*
    Activity component displays data for a single entry in the history for concrete Day and Project and describes what was actually done.

*/

const Activity = ({ activity, editMenuIsShown, updateActivity, showEditMenu, editActivity, deleteActivity }) => {

    Activity.propTypes = {
        activity: propTypes.object,
        updateActivity: propTypes.func,
        showEditMenu: propTypes.func,
        editActivity: propTypes.func,
        deleteActivity: propTypes.func
    }

    let i = 0;

    var date = new Date(null);
    date.setSeconds(activity.duration);
    var duration = date.toISOString().substr(11, 8);

    return (
        <div className="activities-block">
            <div className="duration-wrap activity-duration">
                <p className="activity-name" onKeyDown={(e) => updateActivity(e)} id={activity.id}>{activity.activity}</p>
                <div className="duration-right-part">
                    <p className="time-duration">{duration}</p>
                    <i className="fas fa-ellipsis-v" onClick={showEditMenu}></i>
                    {
                        editMenuIsShown[activity.id] ? (
                            <div className="edit-entry-menu test animated fadeIn faster">
                                <p className="edit-entry-menu-link" onClick={() => editActivity(activity.id) }>Edit</p>
                                <p className="delete-entry-menu-link" onClick={() => deleteActivity()}>Delete</p>
                            </div>
                        ) : (
                            null
                        )
                    }
                </div>
            </div>

            {activity.intervals != null ?
                activity.intervals.map((interval) => {

                    return <p className="interval" key={i++}>{moment(interval.start).local().format("HH:mm:ss")} - {moment
                    (interval.stop).local().format("HH:mm:ss")}</p>

                }) : null}
        </div>
    )
}

export default Activity;