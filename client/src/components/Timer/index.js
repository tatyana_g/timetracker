import React from "react";
import propTypes from 'prop-types';

/*
    Displays timer.
 */

const Timer = ({timer}) => {

    Timer.propTypes = {
        timer: propTypes.string
    }

    return (
        <div>
            <span className="timer">{timer}</span>
        </div>
    )
}

export default Timer;