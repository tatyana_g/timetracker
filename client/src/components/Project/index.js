import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from "../../actions/index";
import lodash from 'lodash';
import Project from "./Project";
import propTypes from 'prop-types';

class ProjectContainer extends React.Component {

    static propTypes = {
        project: propTypes.object,
        day: propTypes.string,
        activitiesAreShown: propTypes.object,
        shouldShowSelectProject: propTypes.bool
    }

    constructor(props) {
        super(props);
        this.activitiesId = lodash.uniqueId();
    }

    /* Shows edit menu for projects */
    showEditMenu = (event) => {
        // close menu after click outside
        document.addEventListener('click', this.closeEditMenu);
        console.log("showEditMenu");
        this.props.showEditMenu(this.props.project.id);
    }

    closeEditMenu = () => {
        this.props.hideEditMenu(this.props.project.id);
        document.removeEventListener('click', this.closeEditMenu);
    }

    toggleActivityBlock = () => {
        this.props.toggleActivityBlock(this.activitiesId);
    }

    showProjectSelect = (event) => {
        console.log("Show project select function in Project");
        // closeProjectSelectMenu() should be called in any case on the next click: selecting project
        // or clicking outside of the menu
        document.addEventListener('click', this.closeProjectSelectMenu);
        this.props.showProjectSelectMenu(this.props.project.id);
    }

    /*  In Project we need it's own show- and closeProjectSelectMenu, because showProjectSelect method is called
     *   after choosing what to display when clicking on projectname: SelectProjectMenu or activities block (this
     *   is choosed by a value of shouldShowSelectProject variable). shouldShowSelectProject prop is changed by
     *   open- and hideEditProject actions.
     */
    closeProjectSelectMenu = (event) => {
        console.log("closeProjectSelectMenu");
        if (!event.target.matches(".project-link") && !event.target.matches(".new-project-input")) {
            // When clicking outside menu hide it
            console.log("Click outside SelectProject menu");
            this.props.hideProjectSelect(this.props.project.id);
            document.removeEventListener('click', this.closeProjectSelectMenu);
            // set shouldShowSelectProject prop value to false, so that the next time user clicks on the
            // projectname, block with activities will be displayed instead of menu for editing projects
            this.props.closeEditProjectMenu();
        }
        else if (event.target.matches(".project-link")) {
            // When selecting new project, hide the menu and update project
            this.props.hideProjectSelect(this.props.project.id);
            document.removeEventListener('click', this.closeProjectSelectMenu);
            const selectedProject = event.target.innerHTML;
            this.props.updateProject(this.props.project.id, selectedProject);
        } else if (event.target.matches('.new-project-input')) {
            // When creating new project, get the new projectname, create new project and then update entry
            const input = event.target;
            input.addEventListener('keyup', this.addProjectAndUpdateEntry);
        }
    }

    addProjectAndUpdateEntry = (target) => {
        if (target.keyCode == 13 ) {
            console.log("Enter");
            const newProject = document.getElementById('project-input').value;
            if (newProject) {
                console.log("Creating project: " + newProject);
                this.props.saveNewProject(newProject);
                // send as a parameters: project id, new project name, and date for efficient searching
                this.props.updateProject(this.props.project.id, newProject);
                document.getElementById("project-input").value = "";
                target.removeEventListener('click', this.addProjectAndUpdateEntry)
            }
        }
    }


    render() {
        return (

        <Project
            shouldShowSelectProject={this.props.shouldShowSelectProject}
            showProjectSelect={this.showProjectSelect}
            toggleActivityBlock={this.toggleActivityBlock}
            project={this.props.project}
            showEditMenu={this.showEditMenu}
            activitiesAreShown={this.props.activitiesAreShown}
            activitiesId={this.activitiesId}
            day={this.props.day}
        />
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        toggleActivityBlock: bindActionCreators(actions.toggleActivityBlock, dispatch),
        showEditMenu: bindActionCreators(actions.showEditMenu, dispatch),
        hideEditMenu: bindActionCreators(actions.hideEditMenu, dispatch),
        closeEditProjectMenu: bindActionCreators(actions.hideEditProject, dispatch),
        showProjectSelectMenu: bindActionCreators(actions.showProjectSelect, dispatch),
        saveNewProject: bindActionCreators(actions.saveNewProject, dispatch),
        hideProjectSelect: bindActionCreators(actions.hideProjectSelect, dispatch),
        updateProject: bindActionCreators(actions.updateProjectEntry, dispatch)
    }
}

function mapStateToProps(state, ownProps) {
    return {
        project: ownProps.project,
        day: ownProps.day,
        activitiesAreShown: state.toggle.activitiesAreShown,
        shouldShowSelectProject: state.toggle.shouldShowSelectProject,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectContainer);