import React from 'react';
import Activity from "../Activity";
import SelectProject from '../SelectProject';
import propTypes from 'prop-types';
import EditMenu from "../EditMenu";

/*
    Project section in a Day component.
 */

const Project = ({ shouldShowSelectProject, showProjectSelect, toggleActivityBlock, project, day, showEditMenu,
    activitiesAreShown, activitiesId }) => {

    Project.propTypes = {
        showProjectSelect: propTypes.func,
        toggleActivityBlock: propTypes.func,
        showEditMenu: propTypes.func,
        editProject: propTypes.func,
        deleteProject: propTypes.func,
        activitiesId: propTypes.string
    }

    let i = 0;

    var date = new Date(null);
    date.setSeconds(project.duration);
    var duration = date.toISOString().substr(11, 8);
    return (
        <div className="projects-block">
            <div className="duration-wrap project-duration">
                <div className="duration-left-part" onClick={shouldShowSelectProject ? showProjectSelect : toggleActivityBlock}>
                    <span className="fas fa-hashtag hashtag-history" id={project.id}></span>
                    <p className="project-name">{project.project}</p>
                    <SelectProject id={project.id} />
                </div>
                <div className="duration-right-part">
                    <p className="time-duration">{duration}</p>
                    <i className="fas fa-ellipsis-v" onClick={showEditMenu}></i>

                <EditMenu objectId={project.id} />
                </div>
            </div>
            <div className="activities">
                {
                    activitiesAreShown[activitiesId] ? (

                        project.activities.map(activity => {
                            return <Activity key={i++} activity={activity} day={day} project={project} />
                        })
                    ) : (
                        null
                    )
                }
            </div>
        </div>
    )
}

export default Project;
