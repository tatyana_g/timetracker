import React from 'react';
import {connect} from "react-redux";
import EditMenu from "./EditMenu";
import * as actions from '../../actions';
import {bindActionCreators} from "redux";
import propTypes from 'prop-types';


class EditMenuContainer extends React.Component {

    static propTypes = {
        editMenuIsShown: propTypes.object,
        objectId: propTypes.string
    }

    /*  Shows # sign which indicates that project is editable and dispatches openEditProject, which changes
     *  shouldShowSelectProject prop to true. This prop is used when deciding whether to show
     *  activities block or SelectProject menu after click on the project name in History.
     */
    editProject = () => {
        const element = document.getElementById(this.props.objectId);
        element.classList.add("show");
        // Hide hashtag in any case on the next click: clicking anywhere or
        // opening SelectProject menu by clicking on project name
        document.addEventListener('click', this.hideHashTag);
        this.props.openEditProjectMenu();
    }

    /*  After hiding the hashtag we set shouldShowSelectProject to false so that
     *  SelectProject menu is not shown again until moment when user clicks on "Edit" and then on hashtag.
     */
    hideHashTag = () => {
        console.log("HideHashTag");
        const element = document.getElementById(this.props.objectId);
        element.classList.remove("show");
        document.removeEventListener('click', this.hideHashTag);
    }

    deleteProject = () => {
        this.props.removeProjectEntry(this.props.objectId);
    }

    render() {

        return (
            <span>
                {
                    this.props.editMenuIsShown[this.props.objectId] ? (
                        <EditMenu editObject={this.editProject} deleteObject={this.deleteProject}/>
                    ) : null
                }
            </span>
        )
    }
}


function mapStateToProps(state, ownProps) {
    return {
        editMenuIsShown: state.toggle.editMenuIsShown,
        objectId: ownProps.objectId
    }
}

function mapDispatchToProps(dispatch) {
    return {
        openEditProjectMenu: bindActionCreators(actions.showEditProject, dispatch),
        removeProjectEntry: bindActionCreators(actions.removeProjectEntry, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (EditMenuContainer);
