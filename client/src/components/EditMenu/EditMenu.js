import React from 'react';

/*
    Menu to edit or delete history entries
 */
const EditMenu = ({editObject, deleteObject}) => {

    return (
        <div className="edit-entry-menu animated fadeIn faster">
            <p className="edit-entry-menu-link" onClick={editObject}>Edit</p>
            <p className="edit-entry-menu-link" onClick={deleteObject}>Delete</p>
        </div>
    )
}


export default EditMenu;