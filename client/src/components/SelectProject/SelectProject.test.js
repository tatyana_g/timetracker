import React from 'react';
import Enzyme from 'enzyme';
import { shallow, configure } from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build/index";
import SelectProject from "./SelectProject";

Enzyme.configure({ adapter: new Adapter() });

describe("SelectProject component", () => {

    const projects = [
        {
            "_id": 123, name: "One", sumOfTime: 48
        },
        {
            "_id": 456, name: "Two", sumOfTime: 12
        }
    ]


    it ("renders", () => {
        const wrapper = shallow(<SelectProject selectProjectIsShown={true} elementId={123} />);
        expect(wrapper.exists()).toBe(true);
    });

    it ("should display selected project if the project was selected", () => {
        const wrapper = shallow(<SelectProject elementId={"main-menu"} selectProjectIsShown={false} selectedProject={"Test"} />);
        const project = wrapper.find('.select-or-choose-project').text();
        expect(project).toEqual("Test");
    })

    it ("should display a \"Choose your project\" in TimerPanel if no project was selected", () => {
        const wrapper = shallow(<SelectProject selectProjectIsShown={false} elementId={"main-menu"} />);
        const project = wrapper.find(".select-or-choose-project").text();
        expect(project).toEqual("Choose your project");
    })

    it ("should display select project menu on request", () => {
        const wrapper = shallow(<SelectProject selectProjectIsShown={{'main-menu': true}} elementId={"main-menu"} projects={projects}/>);
        expect(wrapper.find(".hidden-menu-select-project").exists()).toBe(true);
        expect(wrapper.find(".project-link").first().text()).toEqual("One");
    })
})
