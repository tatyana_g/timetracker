import * as actions from '../../actions/index';
import {bindActionCreators} from "redux";
import React from "react";
import {connect} from "react-redux";
import SelectProject from "./SelectProject";
import propTypes from 'prop-types';
import { showProjectSelect } from '../../actions/toggle/index';
import { hideProjectSelect } from "../../actions/index";
import { selectProject } from "../../actions/index";
import { saveNewProject } from "../../actions/index";
import { hideEditProject } from "../../actions/index";


/*
   Menu for selecting a project when editing an existing entry or choosing a project on the start of tracking.
*/

class SelectProjectContainer extends React.Component {

    static propTypes = {
        elementId: propTypes.string,
        selectProjectIsShown: propTypes.object,
        selectedProject: propTypes.string,
        projects: propTypes.array
    }

    showMenu = (event) => {
        console.log("showMenu in SelectProject");
        document.addEventListener('click', this.closeMenu);
        this.props.showProjectSelect(this.props.id);
    };

    closeMenu = (event) => {
        console.log("closeMenu in SelectProject");
        const dropdownInput = document.getElementById('project-input');
        /* DropdownInput === null condition is for situations when user goes to another
        * page leaving the selectProject menu open */
        if (dropdownInput === null || !dropdownInput.contains(event.target)) {
            this.props.hideProjectSelect(this.props.id);
            document.removeEventListener('click', this.closeMenu);
            this.props.closeEditProjectMenu();
        }
    }

    /*  If TimerPanel invokes this component then id = "main-menu" and new selectedProject value should be set.
     *  If a component is used while editing existing entry of project then menu should be hidden, and not be
     *  displayed again until moment when user clicks on "Edit" and then on hashtag (See the Project component).
     */
    selectProject = (project) => {
        console.log("Select project: " + project);
        console.log(this.props.id);

        if (this.props.id === "main-menu") {
            this.props.selectProject(project);
        } else {
            this.props.hideProjectSelect(this.props.id);
            document.removeEventListener('click', this.closeMenu);
            this.props.closeEditProjectMenu();
        }
    }

    handleInput = (e) => {
        // This only applies to SelectProject in the Header
        if (this.props.id === "main-menu") {
            if (e.keyCode == 13 || e.type == "click") {
                const project = document.getElementById('project-input').value;
                if (project) {
                    console.log("Creating project: " + project);
                    this.props.saveNewProject(project);
                    document.getElementById("project-input").value = "";
                }
            }
        }
    }

    render() {
        return (

        <SelectProject
            elementId={this.props.elementId}
            selectedProject={this.props.selectedProject}
            showMenu={this.showMenu}
            selectProjectIsShown={this.props.selectProjectIsShown}
            projects={this.props.projects}
            selectProject={this.selectProject}
            handleInput={this.handleInput}
        />

        )
    }
}


function mapStateToProps(state, ownProps) {
    return {
        elementId: ownProps.id,
        selectProjectIsShown: state.toggle.selectProjectIsShown,
        selectedProject: state.projects.selectedProject,
        projects: state.projects.projects
    }
}

export default connect(mapStateToProps, {
    showProjectSelect,
    hideProjectSelect,
    selectProject,
    saveNewProject,
    closeEditProjectMenu: hideEditProject})(SelectProjectContainer);

