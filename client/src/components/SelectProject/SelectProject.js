import React from 'react';
import propTypes from 'prop-types';

const SelectProject = ({elementId, selectedProject, showMenu, selectProjectIsShown, projects,
                           selectProject, handleInput}) => {

    SelectProject.propTypes = {
        showMenu: propTypes.func,
        selectProject: propTypes.func,
        handleInput: propTypes.func
    }

    return (
        <div className="select-project">
            { elementId === "main-menu" ? (

                /* Show project name or only a # sign depending on whether the user selected project or not */
                selectedProject ? (
                    /* if a project was selected then display it */
                    <div>
                        <span className="fas fa-hashtag hashtag" onClick={showMenu}></span>
                        <span className="select-or-choose-project" onClick={showMenu}>{selectedProject}</span>
                    </div>
                ) : (
                    /* otherwise display a # sign */
                    <div>
                        <span className="fas fa-hashtag hashtag" onClick={showMenu}></span>
                        <span className="select-or-choose-project" onClick={showMenu}>Choose your project</span>
                    </div>
                )
            ) : ( null ) }
            {
                /* Show or hide selecting project menu  */
                selectProjectIsShown[elementId] ? (
                    /* show dropdown menu for selecting project */
                    <div className={(elementId === "main-menu" ? 'hidden-menu-select-project animated fadeIn' :
                        'hidden-menu-select-project animated fadeIn menu-edit-project')}>
                        {
                            projects.map(project => (
                                <div className="project-link" key={project.name} onClick={() => {selectProject(project.name) }}>
                                    {project.name}
                                </div>
                            ))
                        }
                        <div className="new-project">
                            <img src={require('../../styles/img/plus.png')} className="icon-plus" onClick={handleInput} />
                            <input id="project-input" type="text" className="new-project-input" placeholder="Create new project..."
                                   onKeyDown={handleInput} />
                        </div>
                    </div>
                ) : (
                    null
                )
            }
        </div>
    )
}

export default SelectProject;