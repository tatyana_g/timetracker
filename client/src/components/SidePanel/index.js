import React from 'react';
import {siteSections} from "../../constants/siteSections";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import propTypes from 'prop-types';


/*
    A section on the left side which contains a site menu.
 */

function MenuItem ({section}) {

    MenuItem.propTypes = {
        section: propTypes.string
    }

    return (
        <Link to={`${section}`} className="menu-item">
            {section}
        </Link>
    )
}

class SidePanel extends React.Component {

    render() {
        return (
                <div className="side-panel">
                    <div className="side-panel-padding">
                    <i className="far fa-clock"></i>
                    <div className="logo">TimeTracker</div>
                    <div className="menuList">
                        {siteSections.map((section, key) => {
                            return <MenuItem section={section} key={key} />
                        })}
                    </div>
                </div>
                </div>

        )
    }
}


export default connect()(SidePanel);