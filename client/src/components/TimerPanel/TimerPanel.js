import React from 'react';
import SelectProject from "../SelectProject";
import Timer from "../Timer";
import propTypes from 'prop-types';
import TimerButton from "./TimerButton";

export const TimerPanel = ({handleInput, selectedProject, timer, handleTrackingStatus, isTracking}) => {

    TimerPanel.propTypes = {
        handleInput: propTypes.func,
        selectedProject: propTypes.string,
        timer: propTypes.string,
        handleTrackingStatus: propTypes.func,
        isTracking: propTypes.bool
    }

    return (
        <div className="timer-panel-wrapper">
            <div className="timer-panel">
                <div>
                    <input id="activity-input" className="activity-input" type="text"
                           placeholder="What are you going to do? "
                           onKeyDown={handleInput}/>
                </div>

                <div className="timer-panel-right">
                    <SelectProject selectedProject={selectedProject} id={"main-menu"}/>
                    <Timer timer={timer}/>
                    <TimerButton handleTrackingStatus={handleTrackingStatus} isTracking={isTracking}/>
                </div>
            </div>
        </div>
    )
}

export default TimerPanel;