import React from 'react';
import {connect} from "react-redux";
import * as actions from '../../actions/index';
import {bindActionCreators} from "redux";
import TimerPanel from "./TimerPanel";
import propTypes from 'prop-types';

/*
    Panel with a timer at the top of the page, containing an input field for activity and a timer.
 */

class TimerPanelContainer extends React.Component {

    static propTypes = {
        selectedProject: propTypes.string,
        isTracking: propTypes.bool,
        timer: propTypes.string,
        start: propTypes.string,
        stop: propTypes.string
    }

    /*
       Start and stop tracking on start/stop button
     */
    handleTrackingStatus = () => {
        const activity = document.getElementById('activity-input').value;
        const { isTracking, selectedProject, start, stop } = this.props;
        if (isTracking) {
            this.props.stopTracking(activity, selectedProject, start);
        }
        else {
            const activity = document.getElementById('activity-input').value;
            this.props.startTracking(activity, this.props.selectedProject, start, stop);
        }
    }

    /*
        Get activity's name from input and start tracking on Enter key down
     */
    handleInput = (e) => {
        if (e.keyCode == 13) {
            if (!this.props.isTracking) {
                const activity = document.getElementById('activity-input').value;
                console.log(activity);
                this.props.startTracking(activity, this.props.selectedProject);
            }
        }
    }

    render() {
        return (
            <TimerPanel
                handleInput={this.handleInput}
                selectedProject={this.props.selectedProject}
                timer={this.props.timer}
                handleTrackingStatus={this.handleTrackingStatus}
                isTracking={this.props.isTracking}
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        selectedProject: state.projects.selectedProject,
        isTracking: state.tracking.isTracking,
        timer: state.tracking.timer,
        start: state.tracking.start,
        stop: state.tracking.stop
    }
}

function mapDispatchToProps(dispatch) {
    return {
        startTracking: bindActionCreators(actions.startTracking, dispatch),
        stopTracking: bindActionCreators(actions.stopTracking, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TimerPanelContainer);