import classNames from "classnames";
import React from 'react';
import propTypes from 'prop-types';

/*
    TimerButton manages start/stop button logic and presentation.
 */

function TimerButton({handleTrackingStatus, isTracking}) {

    TimerButton.propTypes = {
        handleTrackingStatus: propTypes.func,
        isTracking: propTypes.bool
    }

    const buttonActiveClass = classNames({
        'fas fa-play': !isTracking,
        'fas fa-stop': isTracking
    });

    return(
        <div>
            <span className={buttonActiveClass} onClick={handleTrackingStatus}></span>
        </div>
    )
}

export default TimerButton;