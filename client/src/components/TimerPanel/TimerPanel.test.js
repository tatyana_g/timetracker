import React from 'react';
import Enzyme from 'enzyme';
import { configure, shallow, mount } from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build/index";
import TimerPanel from "./TimerPanel";

Enzyme.configure({ adapter: new Adapter() });

describe("TimerPanel component", () => {

    it("should render", () => {
        const wrapper = shallow(<TimerPanel />);
        expect(wrapper.exists()).toBe(true);
        const input = wrapper.find('.activity-input');
        expect(input.exists()).toBe(true);
    })

    it("should handle every key press", () => {
        const handleInput = jest.fn();
        const wrapper = shallow(<TimerPanel handleInput={handleInput} />);
        const input = wrapper.find('.activity-input').simulate('keydown');
        expect(handleInput).toHaveBeenCalledTimes(1);
    })

})
