import React from 'react';
import Day from "../Day";
import propTypes from 'prop-types';

/*
    Contains all history of tracking time, separated by days.

    Day section in the History shows overall information for the concrete day, containing information about
    the projects that were done this day. Every Project contains entries for Activities - each of them shows what
    was done actually with the time spent on it.

 */
const History = ({history}) => {

     History.propTypes = {
        history: propTypes.arrayOf(propTypes.object)
    }

    return (
        <div className="history">
            {history.map( (entry) => {
                return <Day entry={entry} key={entry._id} />
            } )}
        </div>
    )
}

export default History;
