import React from 'react';
import { shallow, configure } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build/index";
import History from './History';

Enzyme.configure({ adapter: new Adapter() });

describe("History component", () => {

    const history = [
            {
                _id: '5b556fb8f32846603cae2003',
                day: '2018-12-23T00:00:00.000Z',
                projects: [
                    {
                        project: 'Work',
                        id: 'ABC',
                        activities: [
                            {
                                activity: 'New feature in my project ',
                                duration: 12,
                                id: '567',
                                intervals: [
                                    {
                                        start: '2018-12-23T06:03:30.737Z',
                                        stop: '2018-12-23T06:03:36.387Z'
                                    }
                                ]
                            }
                        ],
                        duration: 12
                    }
                ],
                duration: 12
            }
        ]

    it ("renders", () => {
        const wrapper = shallow(<History history={history} />);
        expect(wrapper.exists()).toBe(true);
    });

});