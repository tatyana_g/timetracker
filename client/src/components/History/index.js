import React from 'react';
import {connect} from "react-redux";
import * as actions from '../../actions/index';
import {bindActionCreators} from "redux";
import History from "./History";
import propTypes from 'prop-types';

/*
    Shows all history entries of tracking on the main window.
 */

class HistoryContainer extends React.Component {

    static propTypes = {
        stop: propTypes.string,
        history: propTypes.arrayOf(propTypes.object)
    }

    componentDidMount() {
        this.props.fetchHistory();
        this.props.fetchProjects();
    }

    render() {
        const history = Array.from(this.props.history);
        console.log("History: ");
        console.log(history);
        return (
            <History history={history} />
        )
    }
}

function mapStateToProps(state) {
    return {
        stop: state.tracking.stop,
        history: state.history.allEntries
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchHistory: bindActionCreators(actions.fetchAllEntries, dispatch),
        fetchProjects: bindActionCreators(actions.fetchProjects, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(HistoryContainer);
