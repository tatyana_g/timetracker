import React from 'react';
import Calendar from "./Calendar";
import * as actions from "../../actions";
import {bindActionCreators} from "redux";
import DayPicker, {DateUtils} from 'react-day-picker';
import { connect } from 'react-redux';
import propTypes from 'prop-types';

class CalendarContainer extends React.Component {

    static propTypes = {
        data: propTypes.arrayOf(propTypes.object),
        range: propTypes.object
    }

    handleDayClick = (day) => {

        // if range.from was defined, then set range.to to 23:59:59
        if (this.props.range.from != undefined) {
            day.setHours(23);
            day.setMinutes(59);
            day.setSeconds(59);
            // if range.from wasn't defined, then set range.from to 00:00:00
        } else if (this.props.range.from == undefined) {
            day.setHours(0);
            day.setMinutes(0);
            day.setSeconds(0);
        }
        const range = DateUtils.addDayToRange(day, this.props.range);
        this.props.setRange(range);
        console.log(range);
        const project = document.getElementById("select-project-for-chart").value;
        // fetch entries only if range.to was defined
        if (range.to != undefined) {
            this.props.fetchEntries(new Date(range.from), new Date(range.to), project);
        }
    }

    handleResetClick = () => {
        this.props.setRange({ from: undefined, to: undefined });
    }

    render() {

        const range = this.props.range;

        const from = range.from;
        const to = range.to;
        const modifiers = {from: from, to: to};

        return (
            <Calendar
                from={from}
                to={to}
                modifiers={modifiers}
                handleResetClick={this.handleResetClick}
                handleDayClick={this.handleDayClick}
            />
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchEntries: bindActionCreators(actions.fetchEntries, dispatch),
        setRange: bindActionCreators(actions.setRange, dispatch)
    }
}

function mapStateToProps(state) {
    return {
        data: state.dashboard.data,
        range: state.dashboard.range
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarContainer);