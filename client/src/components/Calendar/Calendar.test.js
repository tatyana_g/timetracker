import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build/index";
import Calendar from "./Calendar";
import Enzyme from 'enzyme';

Enzyme.configure({ adapter: new Adapter() });

describe("Calendar component", () => {

    it("should be rendered", () => {
        const wrapper = shallow(<Calendar/>);
        expect(wrapper.exists()).toBe(true);
    })

    it("resets all data after clicking on reset", () => {
        const handleReset = jest.fn();
        const wrapper = shallow(<Calendar handleResetClick={handleReset} />);
        wrapper.find('.calendar-header.link').simulate('click');
        expect(handleReset).toHaveBeenCalledTimes(1);
    })
})