import React from 'react';
import DayPicker, {DateUtils} from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import propTypes from 'prop-types';

/*
    Shows a calendar on the /dashboard section and allows choosing 'from' and 'to' dates for building a chart.
 */

const Calendar = ({handleResetClick, modifiers, from, to, handleDayClick}) => {

    Calendar.propTypes = {
        handleResetClick: propTypes.func,
        handleDayClick: propTypes.func,
        // modifiers, from, to are the react-day-picker's responsibility
    }

    return (
        <div className="calendar">
            <div>
                <p className="calendar-header">
                    {!from && !to && 'Please select the first day.'}
                    {from && !to && 'Please select the last day.'}
                    {from &&
                    to &&
                    `Selected from ${from.toLocaleDateString()} to ${to.toLocaleDateString()}`}{' '}
                </p>
                <p className="calendar-header link" onClick={() => handleResetClick()}>
                    Reset
                </p>
            </div>
            <DayPicker
                className="Selectable"
                numberOfMonths={2}
                selectedDays={[from, {from, to}]}
                modifiers={modifiers}
                onDayClick={(day) => handleDayClick(day)}
            />
        </div>
        )
}

export default Calendar;