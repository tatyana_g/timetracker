import React from 'react';
import * as actions from '../../actions/index';
import { connect } from 'react-redux';
import {bindActionCreators} from "redux";

class ErrorBoundary extends React.Component {

    componentDidCatch(error, info) {
        this.props.setError(error, info);
    }

    render() {
        if(this.props.error != null) {
            return (
                <div>
                    <h1>Sorry, something went wrong</h1>
                    <p>Information: </p>
                    <p>{this.props.error}</p>
                    <p>{this.props.errorInfo}</p>
                </div>
            );
        }
        return this.props.children;
    }
}

function mapStateToProps(state) {
    return {
        error: state.error.error,
        errorInfo: state.error.errorInfo
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setError: bindActionCreators(actions.setErrorState, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorBoundary);
