import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import SidePanel from "../SidePanel/index";
import Overview from "../Overview/index";
import Projects from "../ProjectsTable/index";
import Dashboard from "../Dashboard/index";


class App extends React.Component {

    render() {
        return (
            <BrowserRouter>
                <div className="app-wrapper">
                    <SidePanel />
                    <Switch>
                        <Route exact path={'/'} component={Overview} />
                        <Route exact path={'/projects'} component={Projects} />
                        <Route exact path={'/dashboard'} component={Dashboard} />
                        <Redirect to={'/'} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

export default App;
