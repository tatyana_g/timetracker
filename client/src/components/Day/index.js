import moment from "moment/moment";
import Project from '../Project';
import React from 'react';
import propTypes from 'prop-types';

/*
    Section in tracker history which contains day information (projects, overall working time spent this day and more
    detailed information on projects).

 */

export default function Day({ entry}) {

    Day.propTypes = {
        entry: propTypes.object
    }

    let i = 0;

    // Seconds => hh:mm:ss
    var date = new Date(null);
    date.setSeconds(entry.duration);
    var duration = date.toISOString().substr(11, 8);

    return (
        <div className="day">

            <div className="duration-wrap">
                <p className="date">{moment(entry.day).format("MMM Do YYYY")}</p>

                <p className="day-duration">
                    {duration}
                </p>
            </div>

            {
                entry.projects.map(project => {
                    return <Project key={i++} project={project} day={entry.day} />;
                })
            }
        </div>
    )
}
