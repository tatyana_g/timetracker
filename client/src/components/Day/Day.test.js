import React from 'react';
import { shallow, configure } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from "enzyme-adapter-react-16/build/index";
import Day from "./index";

Enzyme.configure({ adapter: new Adapter() });

describe("Day component", () => {

    const entry = {
        _id: '5b556fb8f32846603cae2003',
        day: '2018-12-23T00:00:00.000Z',
        projects: [
            {
                project: 'Work',
                id: '890',
                activities: [
                    {
                        activity: 'New feature in my project ',
                        duration: 12,
                        id: '567',
                        intervals: [
                            {
                                start: '2018-12-23T06:03:30.737Z',
                                stop: '2018-12-23T06:03:36.387Z'
                            }
                        ]
                    }
                ],
                duration: 12
            }
        ],
        duration: 12
    };

    it("should be rendered", () => {
        const wrapper = shallow(<Day entry={entry} />);
        expect(wrapper.exists()).toBe(true);
    })

    it("should correctly display a date", () => {
        const wrapper = shallow(<Day entry={entry} />);
        expect(wrapper.find(".date").text()).toEqual("Dec 23rd 2018");
    })

    it("should correctly display a duration", () => {
        const wrapper = shallow(<Day entry={entry} />);
        expect(wrapper.find(".day-duration").text()).toEqual("00:00:12");
    })

})
