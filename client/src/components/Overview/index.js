import React from 'react';
import {connect} from "react-redux";
import TimerPanel from "../TimerPanel/index";
import * as actions from '../../actions/index';
import {bindActionCreators} from "redux";
import History from "../History";
import ErrorBoundary from '../ErrorBoundary';

class Overview extends React.Component {

    componentDidMount() {
        this.props.fetchProjects();
    }

    render() {
        return (
            <div className="overview">
                <ErrorBoundary>
                    <TimerPanel/>
                </ErrorBoundary>

                <ErrorBoundary>
                    <History />
                </ErrorBoundary>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProjects: bindActionCreators(actions.fetchProjects, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(Overview);