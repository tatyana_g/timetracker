import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import SidePanel from "./components/SidePanel/index";
import Overview from "./components/Overview/index.js";
import Projects from "./components/ProjectsTable/index";
import Dashboard from "./components/Dashboard/index";


class App extends React.Component {

    render() {
        return (
            <BrowserRouter>
                <div className="app-wrapper">
                    <SidePanel />
                    <Switch>
                        <Route exact path={'/'} component={Overview} />
                        <Route exact path={'/projects'} component={Projects} />
                        <Route exact path={'/dashboard'} component={Dashboard} />
                        <Redirect to={'/'} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

export default App;
