import React from 'react';
import App from './App';
import {shallow} from "enzyme/build/index";
import Adapter from "enzyme-adapter-react-16/build/index";
import Enzyme from "enzyme/build/index";

Enzyme.configure({ adapter: new Adapter() });


it('renders without crashing', () => {

    const wrapper = shallow(<App />);
    expect(wrapper.exists()).toBe(true);
    const input = wrapper.find('.app-wrapper');
    expect(input.exists()).toBe(true);

});
